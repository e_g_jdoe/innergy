﻿using System.Collections.Generic;
using Innergy.Models;

namespace Innergy.Services {
    public interface IWarehouseService {
        IEnumerable<IWarehouse> GetAll();
        IWarehouse GetOrCreate(string id);
    }
}
