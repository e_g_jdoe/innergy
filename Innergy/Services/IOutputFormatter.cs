﻿using System.Collections.Generic;
using Innergy.Models;

namespace Innergy.Services {
    public interface IOutputFormatter {
        string FormatOutput(IEnumerable<IWarehouse> warehouses);
    }
}
