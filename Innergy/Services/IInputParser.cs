﻿using System.Collections.Generic;
using Innergy.Models;

namespace Innergy.Services {
    public interface IInputParser {
        IEnumerable<InputParserResultItem> Parse(string input);
    }

    public class InputParserResultItem {
        public Material Material { get; set; }
        public IEnumerable<(string warehouseId, int count)> ItemsPerWarehouse { get; set; }
    }
}
