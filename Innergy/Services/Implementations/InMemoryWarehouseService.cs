﻿using System.Collections.Generic;
using Innergy.Models;

namespace Innergy.Services.Implementations {
    public class InMemoryWarehouseService : IWarehouseService {
        private readonly IDictionary<string, IWarehouse> _cache;
        public InMemoryWarehouseService() {
            _cache = new Dictionary<string, IWarehouse>();
        }
        public IEnumerable<IWarehouse> GetAll() {
            return _cache.Values;
        }

        public IWarehouse GetOrCreate(string id) {
            if (_cache.ContainsKey(id))
                return _cache[id];
            return _cache[id] = new Warehouse(id);
        }
    }
}
