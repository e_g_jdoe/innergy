﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Innergy.Models;

namespace Innergy.Services.Implementations {
    public class OutputFormatter : IOutputFormatter {
        public string FormatOutput(IEnumerable<IWarehouse> warehouses) {
            var whs = warehouses.OrderByDescending(x => x.Count()).ThenByDescending(x => x.Id).Select(FormatWarehouse).ToArray();
            var whSeparator = $"{Environment.NewLine}";
            return string.Join(Environment.NewLine, whs).Trim();
        }

        private string FormatWarehouse(IWarehouse warehouse) {
            var sb = new StringBuilder();
            sb.AppendLine($"{warehouse.Id} (total {warehouse.Count()})");
            var materials = warehouse.GetMaterials()?.ToArray() ?? new (Material material, int count)[0];
            foreach (var countPerMat in materials.OrderBy(x => x.material.Id)) {
                sb.AppendLine($"{countPerMat.material.Id}: {countPerMat.count}");
            }
            return sb.ToString();
        }
    }
}
