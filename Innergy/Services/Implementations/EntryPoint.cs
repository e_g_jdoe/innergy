﻿using System;

namespace Innergy.Services.Implementations {
    public class EntryPoint : IEntryPoint {
        private readonly IInputParser _inputParser;
        private readonly IWarehouseService _warehouseService;
        private readonly IOutputFormatter _outputFormatter;
        public EntryPoint(IInputParser inputParser, IWarehouseService warehouseService, IOutputFormatter outputFormatter) {
            _inputParser = inputParser;
            _warehouseService = warehouseService;
            _outputFormatter = outputFormatter;
        }

        public string Run(string input) {
            var items = _inputParser.Parse(input);
            foreach (var inputParserResultItem in items) {
                AppendItems(inputParserResultItem);
            }

            var whs = _warehouseService.GetAll();
            return _outputFormatter.FormatOutput(whs);
        }

        private void AppendItems(InputParserResultItem inputParserResultItem) {
            foreach (var whs in inputParserResultItem.ItemsPerWarehouse) {
                var wh = _warehouseService.GetOrCreate(whs.warehouseId);
                wh.Add(inputParserResultItem.Material, whs.count);
            }
        }
    }
}
