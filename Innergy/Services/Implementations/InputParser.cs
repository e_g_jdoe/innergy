﻿using System;
using System.Collections.Generic;
using System.Linq;
using Innergy.Models;

namespace Innergy.Services.Implementations {
    public class InputParser : IInputParser {
        public IEnumerable<InputParserResultItem> Parse(string input) {
            var lines = input?.Split(Environment.NewLine, StringSplitOptions.RemoveEmptyEntries) ?? new string[0];
            var contentLines = lines.Where(x => !x.StartsWith("#")).ToArray();
            return contentLines.Select(ParseLine);
        }

        private InputParserResultItem ParseLine(string line) {
            var cols = line.Split(";").ToArray();
            return new InputParserResultItem{ Material = new Material{ Id = cols[1], Name = cols[0]}, ItemsPerWarehouse = ProceedParseWarehouse(cols.Last())};
        }

        private IEnumerable<(string warehouseId, int count)> ProceedParseWarehouse(string whs) {
            var whsWithCounts = whs.Split("|");
            return whsWithCounts.Select(whc => whc.Split(",")).Select(whc => (whc.First(), int.Parse(whc.Last())));
        }
    }
}
