﻿namespace Innergy.Services {
    public interface IEntryPoint {
        string Run(string input);
    }
}
