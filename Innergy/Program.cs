﻿using System;
using System.IO;
using System.Text;
using Innergy.Services.Implementations;

namespace Innergy {
    class Program {
        static void Main(string[] args) {
            var inputParser = new InputParser();
            var whService = new InMemoryWarehouseService();
            var outputFormatter = new OutputFormatter();
            var entryPoint = new EntryPoint(inputParser, whService, outputFormatter);
            Console.WriteLine("Paste text and press Ctrl+Z");
            using var sr = new StreamReader(Console.OpenStandardInput(), Console.InputEncoding);
            var input = sr.ReadToEnd();
            Console.WriteLine("==============================================");
            Console.WriteLine(entryPoint.Run(input));
            Console.Read();
        }
    }
}
