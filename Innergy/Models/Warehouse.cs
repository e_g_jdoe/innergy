﻿using System.Collections.Generic;
using System.Linq;

namespace Innergy.Models {
    public class Warehouse : IWarehouse {
        private readonly IDictionary<Material, int> _shelves;
        public Warehouse(string id) {
            Id = id;
            _shelves = new Dictionary<Material, int>();
        }
        public string Id { get; }

        public bool Add(Material material, int count) {
            _shelves[material] = _shelves.ContainsKey(material) ? _shelves[material] + count : count;
            return true;
        }

        public IEnumerable<(Material material, int count)> GetMaterials() {
            return _shelves.Select(x => (x.Key, x.Value));
        }

        public int Count() {
            return _shelves.Values.Sum();
        }

        public int GetMaterialCount(Material material) {
            return _shelves.ContainsKey(material) ? _shelves[material] : 0;
        }
    }
}
