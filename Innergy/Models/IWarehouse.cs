﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innergy.Models {
    public interface IWarehouse {
        string Id { get; }
        bool Add(Material material, int count);
        IEnumerable<(Material material, int count)> GetMaterials();
        int Count();
        int GetMaterialCount(Material material);
    }
}
