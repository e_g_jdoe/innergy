﻿namespace Innergy.Models {
    public class Material {
        public string Id { get; set; }
        public string Name { get; set; }

        public override bool Equals(object obj) {
            return ReferenceEquals(obj, this) || obj is Material loc && Equals(loc);
        }

        protected bool Equals(Material other) {
            return Id == other.Id && Name == other.Name;
        }

        public override int GetHashCode() {
            unchecked {
                return ((Id != null ? Id.GetHashCode() : 0) * 397) ^ (Name != null ? Name.GetHashCode() : 0);
            }
        }
    }
}
