﻿using System.Linq;
using Innergy.Models;
using Xunit;

namespace Innergy.Tests.Models {
    public class WarehouseTests {
        private readonly Warehouse _warehouse;

        public WarehouseTests() {
            _warehouse = new Warehouse("Tests");
        }

        [Fact]
        public void Add_ShouldAddNewMaterial() {
            var material = new Material { Id = "Test Id", Name = "Test name" };
            const int count = 10;
            _warehouse.Add(material, count);
            Assert.Equal(count, _warehouse.GetMaterialCount(material));
        }

        [Fact]
        public void Add_ShouldIncreaseCount() {
            var material = new Material { Id = "Test Id", Name = "Test name" };
            const int count = 10;
            _warehouse.Add(material, count);
            Assert.Equal(count, _warehouse.Count());
        }

        [Fact]
        public void Add_ShouldUseAlreadyAddedMaterial() {
            var material = new Material { Id = "Test Id", Name = "Test name" };
            const int count = 10;
            _warehouse.Add(material, count);
            const int newCount = 5;
            _warehouse.Add(material, newCount);
            Assert.Equal(count + newCount, _warehouse.GetMaterialCount(material));
        }

        [Fact]
        public void Count_ShouldReturnsSumCountOfMaterials() {
            var material1 = new Material { Id = "Test Id 1", Name = "Test name 1" };
            var material2 = new Material { Id = "Test Id 2", Name = "Test name 2" };
            const int count1 = 10;
            const int count2 = 10;
            _warehouse.Add(material1, count1);
            _warehouse.Add(material2, count2);
            Assert.Equal(count1 + count2, _warehouse.Count());
        }

        [Fact]
        public void GetMaterials_ShouldReturnsAllAddedMaterialsWithCounts() {
            var material1 = new Material { Id = "Test Id 1", Name = "Test name 1" };
            var material2 = new Material { Id = "Test Id 2", Name = "Test name 2" };
            const int count1 = 10;
            const int count2 = 10;
            var expectedItems = new (Material material, int count)[] {
                (material1, count1),
                (material2, count2)
            }.ToList();
            expectedItems.ForEach(x=> _warehouse.Add(x.material, x.count));
            ;
            var materials = _warehouse.GetMaterials();
            Assert.True(expectedItems.OrderBy(x=>x.material.Id).SequenceEqual(materials.OrderBy(x=>x.material.Id)));
        }

        [Fact]
        public void GetMaterialCount_ShouldReturnsCountForMaterial() {
            var material = new Material { Id = "Test Id", Name = "Test name" };
            const int count = 10;
            _warehouse.Add(material, count);
            Assert.Equal(count, _warehouse.GetMaterialCount(material));
        }

        [Fact]
        public void GetMaterialCount_ShouldReturnsZeroIfMaterialWasNotAdded() {
            var material = new Material { Id = "Test Id", Name = "Test name" };
            Assert.Equal(0, _warehouse.GetMaterialCount(material));
        }
    }
}
