﻿using System;
using System.Linq;
using Innergy.Models;
using Innergy.Services.Implementations;
using Moq;
using Xunit;

namespace Innergy.Tests.Services {
    public class OutputFormatterTests {
        private readonly OutputFormatter _outputFormatter;
        public OutputFormatterTests() {
            _outputFormatter = new OutputFormatter();
        }
        [Fact]
        public void FormatOutput_WarehousesShouldBeDescSortedByCount() {
            const string whName1 = "Tests 1";
            const int whCount1 = 50;
            const string whName2 = "Tests 2";
            const int whCount2 = 75;
            var warehouse1 = new Mock<IWarehouse>();
            warehouse1.SetupGet(x => x.Id).Returns(whName1);
            warehouse1.Setup(x => x.Count()).Returns(whCount1);

            var warehouse2 = new Mock<IWarehouse>();
            warehouse2.SetupGet(x => x.Id).Returns(whName2);
            warehouse2.Setup(x => x.Count()).Returns(whCount2);

            var result = _outputFormatter.FormatOutput(new[] { warehouse1.Object, warehouse2.Object }).Trim();
            Assert.True(result.IndexOf($"{whName2} (total {whCount2})", StringComparison.Ordinal) < result.IndexOf($"{whName1} (total {whCount1})", StringComparison.Ordinal));
        }

        [Fact]
        public void FormatOutput_WarehousesShouldBeSortedByIdDescIfCountIsTheSame() {
            const string whName1 = "ATests 1";
            const int whCount1 = 50;
            const string whName2 = "BTests 2";
            const int whCount2 = 50;
            var warehouse1 = new Mock<IWarehouse>();
            warehouse1.SetupGet(x => x.Id).Returns(whName1);
            warehouse1.Setup(x => x.Count()).Returns(whCount1);

            var warehouse2 = new Mock<IWarehouse>();
            warehouse2.SetupGet(x => x.Id).Returns(whName2);
            warehouse2.Setup(x => x.Count()).Returns(whCount2);

            var result = _outputFormatter.FormatOutput(new[] { warehouse1.Object, warehouse2.Object });
            Assert.True(result.IndexOf($"{whName1} (total {whCount1})", StringComparison.Ordinal) > result.IndexOf($"{whName2} (total {whCount2})", StringComparison.Ordinal));
        }

        [Fact]
        public void FormatOutput_WarehousesShouldBeSeparatedByNewLine() {
            const string whName1 = "Tests 1";
            const int whCount1 = 50;
            const string whName2 = "Tests 2";
            const int whCount2 = 75;
            var warehouse1 = new Mock<IWarehouse>();
            warehouse1.SetupGet(x => x.Id).Returns(whName1);
            warehouse1.Setup(x => x.Count()).Returns(whCount1);

            var warehouse2 = new Mock<IWarehouse>();
            warehouse2.SetupGet(x => x.Id).Returns(whName2);
            warehouse2.Setup(x => x.Count()).Returns(whCount2);

            var result = _outputFormatter.FormatOutput(new[] { warehouse1.Object, warehouse2.Object }).Split($"{Environment.NewLine}{Environment.NewLine}", StringSplitOptions.RemoveEmptyEntries);
            Assert.True(result.First().Trim().Equals($"{whName2} (total {whCount2})") && result.Last().Trim().Equals($"{whName1} (total {whCount1})"));
        }

        [Fact]
        public void FormatOutput_ShouldReturnsIdOfTheWarehouse() {
            const string whName = "Tests";
            var warehouse = new Mock<IWarehouse>();
            warehouse.SetupGet(x => x.Id).Returns(whName);
            var result = _outputFormatter.FormatOutput(new[] { warehouse.Object });
            Assert.StartsWith(whName, result);
        }

        [Fact]
        public void FormatOutput_ShouldNoEndWithWhiteSpace() {
            const string whName = "Tests";
            var warehouse = new Mock<IWarehouse>();
            warehouse.SetupGet(x => x.Id).Returns(whName);
            var result = _outputFormatter.FormatOutput(new[] { warehouse.Object });
            Assert.Equal(result, result.Trim());
        }

        [Fact]
        public void FormatOutput_ShouldReturnsCountIfTheItemsThatAreStoredInWarehouse() {
            const string whName = "Tests";
            const int whCount = 123;
            var warehouse = new Mock<IWarehouse>();
            warehouse.SetupGet(x => x.Id).Returns(whName);
            warehouse.Setup(x => x.Count()).Returns(whCount);
            var result = _outputFormatter.FormatOutput(new[] { warehouse.Object });
            Assert.StartsWith($"{whName} (total {whCount})", result);
        }

        [Fact]
        public void FormatOutput_ShouldReturnsMaterialsFromWarehouse() {
            const string whName = "Tests";
            var warehouse = new Mock<IWarehouse>();
            warehouse.SetupGet(x => x.Id).Returns(whName);
            var material = new Material{ Id = "Test Id", Name = "Test 1"};
            warehouse.Setup(x => x.GetMaterials()).Returns(new[] {(material, 12)});
            var result = _outputFormatter.FormatOutput(new[] { warehouse.Object }).Split(Environment.NewLine, StringSplitOptions.RemoveEmptyEntries).Skip(1).Single();
            Assert.StartsWith(material.Id, result);
        }

        [Fact]
        public void FormatOutput_ShouldReturnsMaterialsWithCountSortedByName() {
            const string whName = "Tests";
            var warehouse = new Mock<IWarehouse>();
            warehouse.SetupGet(x => x.Id).Returns(whName);
            var material1 = new Material { Id = "BBB", Name = "Test 1" };
            var material2 = new Material { Id = "AAA", Name = "Test 1" };
            warehouse.Setup(x => x.GetMaterials()).Returns(new[] { (material1, 12), (material2, 1) });
            var result = _outputFormatter.FormatOutput(new[] { warehouse.Object }).Split(Environment.NewLine, StringSplitOptions.RemoveEmptyEntries).Skip(1).ToArray();
            Assert.True(result.First().Equals($"{material2.Id}: 1") && result.Last().Equals($"{material1.Id}: 12"));
        }
    }
}
