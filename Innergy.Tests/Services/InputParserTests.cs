﻿using System.Linq;
using Innergy.Services.Implementations;
using Xunit;

namespace Innergy.Tests.Services {
    public class InputParserTests {
        private readonly InputParser _inputParser;
        public InputParserTests() {
            _inputParser = new InputParser();
        }

        [Fact]
        public void Parse_ShouldReturnEmptyCollectionIfInputIsEmptyString() {
            var result = _inputParser.Parse("");
            Assert.Empty(result);
        }

        [Fact]
        public void Parse_ShouldReturnEmptyCollectionIfInputIsNull() {
            var result = _inputParser.Parse(null);
            Assert.Empty(result);
        }

        [Fact]
        public void Parse_ShouldReturnsMaterialNameFromGivenLine() {
            const string name = "Cherry Hardwood Arched Door - PS";
            var input = $"{name};COM-100001;WH-A,5|WH-B,10";
            var result = _inputParser.Parse(input).Single();
            Assert.Equal(name, result.Material.Name);
        }

        [Fact]
        public void Parse_ShouldReturnsMaterialIdFromGivenLine() {
            const string id = "COM-100001";
            var input = $"Cherry Hardwood Arched Door - PS;{id};WH-A,5|WH-B,10";
            var result = _inputParser.Parse(input).Single();
            Assert.Equal(id, result.Material.Id);
        }

        [Fact]
        public void Parse_ShouldReturnsAnsManyWarehousesAsThereIsWarehousesInGivenLine() {
            var warehouses = Enumerable.Range(0, 10).Select(x => $"WH-{x},{x}").ToList();
            var input = $"Cherry Hardwood Arched Door - PS;COM-100001;{string.Join("|", warehouses)}";
            var result = _inputParser.Parse(input).Single();
            Assert.Equal(warehouses.Count, result.ItemsPerWarehouse.Count());
        }

        [Fact]
        public void Parse_ShouldReturnsNumberOfItemsInWarehouse() {
            const int count = 125;
            var input = $"Cherry Hardwood Arched Door - PS;COM-100001;WH-1,{count}";
            var result = _inputParser.Parse(input).Single();
            Assert.Equal(count, result.ItemsPerWarehouse.Single(z => z.warehouseId == "WH-1").count);
        }

        [Fact]
        public void Parse_ShouldReturnsWarehouseNames() {
            const string id = "TEST WH";
            var input = $"Cherry Hardwood Arched Door - PS;COM-100001;{id},125";
            var result = _inputParser.Parse(input).Single();
            Assert.Equal(id, result.ItemsPerWarehouse.Single().warehouseId);
        }

        [Fact]
        public void Parse_ShouldReturnsAsManyItemAsThereIsValidLines() {
            var items = Enumerable.Range(0, 10).Select(x => $"Test {x};COM-{x};WH-{x},5").ToArray();
            var result = _inputParser.Parse(string.Join("\r\n", items));
            Assert.Equal(items.Length, result.Count());
        }

        [Fact]
        public void Parse_ShouldIgnoreLinesThatBeginsWithHash() {
            const string ignoreLine = @"#123456";
            var parsedItems = _inputParser.Parse(ignoreLine);
            Assert.Equal(0, parsedItems.Count());
        }
    }
}
