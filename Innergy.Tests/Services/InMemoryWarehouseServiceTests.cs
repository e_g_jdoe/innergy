﻿using System.Linq;
using System.Reflection;
using Innergy.Models;
using Innergy.Services.Implementations;
using Moq;
using Xunit;

namespace Innergy.Tests.Services {
    public class InMemoryWarehouseServiceTests {
        private readonly InMemoryWarehouseService _warehouseService;
        public InMemoryWarehouseServiceTests() {
            _warehouseService = new InMemoryWarehouseService();
        }

        [Fact]
        public void GetOrCreate_ShouldReturnsWarehouseThatWasAlreadyAdded() {
            const string id = "Test id";
            var warehouse = _warehouseService.GetOrCreate(id);
            var warehouse2 = _warehouseService.GetOrCreate(id);
            Assert.True(ReferenceEquals(warehouse, warehouse2));
        }

        [Fact]
        public void GetOrCreate_ShouldReturnsNewWarehouseIfWarehouseOfGivenIdWasNotPreviouslyAdded() {
            const string id = "Test id";
            var warehouse = _warehouseService.GetOrCreate(id);
            Assert.Equal(id, warehouse.Id);
        }

        [Fact]
        public void GetAll_ShouldReturnsAllAddedWarehouses() {
            var warehouseIds = Enumerable.Range(0, 10).Select(x => $"Test {x}").ToList();
            warehouseIds.ForEach(x => _warehouseService.GetOrCreate(x));
            var warehouses = _warehouseService.GetAll();
            Assert.True(warehouseIds.OrderBy(x => x).SequenceEqual(warehouses.Select(x => x.Id).OrderBy(x => x)));
        }
    }
}
