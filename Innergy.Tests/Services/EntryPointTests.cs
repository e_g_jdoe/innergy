﻿using System.Collections.Generic;
using Innergy.Models;
using Innergy.Services;
using Innergy.Services.Implementations;
using Moq;
using Xunit;

namespace Innergy.Tests.Services {
    public class EntryPointTests {
        private readonly EntryPoint _entryPoint;
        private readonly Mock<IInputParser> _inputParser;
        private readonly Mock<IWarehouseService> _warehouseService;
        private readonly Mock<IOutputFormatter> _outputFormatter;
        public EntryPointTests() {
            _inputParser = new Mock<IInputParser>();
            _warehouseService = new Mock<IWarehouseService>();
            _outputFormatter = new Mock<IOutputFormatter>();
            _entryPoint = new EntryPoint(_inputParser.Object, _warehouseService.Object, _outputFormatter.Object);
        }

        [Fact]
        public void Run_ShouldInvokesInputParserWithInputProvidedAsAParam() {
            const string param = "Test input";
            _entryPoint.Run(param);
            _inputParser.Verify(x => x.Parse(param), Times.Once);
        }

        [Fact]
        public void Run_ShouldInvokesWarehouseServiceGetOrCreateMethodAsManyTimesAsThereWasItemsInAllResultItem() {
            const string id = "wh-1";
            var items = new (string warehouseId, int count)[] { (id, 123), (id, 123), (id, 123) };
            _warehouseService.Setup(x => x.GetOrCreate(It.IsAny<string>())).Returns(new Mock<IWarehouse>().Object);
            _inputParser.Setup(x => x.Parse(It.IsAny<string>())).Returns(new[] {
                new InputParserResultItem { Material = new Material{ Id = "1", Name = "1"}, ItemsPerWarehouse = items }, 
                new InputParserResultItem { Material = new Material{ Id = "2", Name = "2"}, ItemsPerWarehouse = items }
            });
            _entryPoint.Run("");
            _warehouseService.Verify(x => x.GetOrCreate(It.IsAny<string>()), Times.Exactly(items.Length * 2));
        }

        [Fact]
        public void Run_ShouldInvokesWarehouseServiceGetAllMethod() {
            _entryPoint.Run("");
            _warehouseService.Verify(x => x.GetAll(), Times.Once);
        }

        [Fact]
        public void Run_ShouldInvokesOutputFormatterWithWarehousesProvidedByWarehouseService() {
            var items = new[] { new Mock<IWarehouse>().Object, new Mock<IWarehouse>().Object };
            _warehouseService.Setup(x => x.GetAll()).Returns(items);
            _entryPoint.Run("");
            _outputFormatter.Verify(x => x.FormatOutput(items), Times.Once);
        }

        [Fact]
        public void Run_ShouldReturnsWhateverOutputFormatterReturn() {
            const string expectedResult = "Expected result";
            _outputFormatter.Setup(x => x.FormatOutput(It.IsAny<IEnumerable<IWarehouse>>())).Returns(expectedResult);
            var result = _entryPoint.Run("");
            Assert.Equal(expectedResult, result);
        }

        [Fact]
        public void Run_ShouldInvokesWarehouseServiceGetOrCreateMethodWithWarehouseIdProvidedByInputParser() {
            const string id = "wh-1";
            var items = new (string warehouseId, int count)[] { (id, 123) };
            _warehouseService.Setup(x => x.GetOrCreate(It.IsAny<string>())).Returns(new Mock<IWarehouse>().Object);
            _inputParser.Setup(x => x.Parse(It.IsAny<string>())).Returns(new[] { new InputParserResultItem { ItemsPerWarehouse = items } });
            _entryPoint.Run("");
            _warehouseService.Verify(x => x.GetOrCreate(id), Times.Once);
        }

        [Fact]
        public void Integration()
        {
            var input = @"# Material inventory initial state as of Jan 01 2018
# New materials
Cherry Hardwood Arched Door - PS;COM-100001;WH-A,5|WH-B,10
Maple Dovetail Drawerbox;COM-124047;WH-A,15
Generic Wire Pull;COM-123906c;WH-A,10|WH-B,6|WH-C,2
Yankee Hardware 110 Deg. Hinge;COM-123908;WH-A,10|WH-B,11
# Existing materials, restocked
Hdw Accuride CB0115-CASSRC - Locking Handle Kit - Black;CB0115-CASSRC;WH-C,13|WH-B,5
Veneer - Charter Industries - 3M Adhesive Backed - Cherry 10mm - Paper Back;3M-Cherry-10mm;WH-A,10|WH-B,1
Veneer - Cherry Rotary 1 FSC;COM-123823;WH-C,10
MDF, CARB2, 1 1/8"";COM-101734;WH-C,8";

            var expectedResult = @"WH-A (total 50)
3M-Cherry-10mm: 10
COM-100001: 5
COM-123906c: 10
COM-123908: 10
COM-124047: 15

WH-C (total 33)
CB0115-CASSRC: 13
COM-101734: 8
COM-123823: 10
COM-123906c: 2

WH-B (total 33)
3M-Cherry-10mm: 1
CB0115-CASSRC: 5
COM-100001: 10
COM-123906c: 6
COM-123908: 11";

            var ep = new EntryPoint(new InputParser(),  new InMemoryWarehouseService(), new OutputFormatter());
            var result = ep.Run(input);
            Assert.Equal(expectedResult, result);
        }
    }
}
